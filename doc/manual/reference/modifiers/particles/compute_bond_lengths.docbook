<?xml version="1.0" encoding="utf-8"?>
<section version="5.0"
         xsi:schemaLocation="http://docbook.org/ns/docbook http://docbook.org/xml/5.0/xsd/docbook.xsd"
         xml:id="particles.modifiers.compute_bond_lengths"
         xmlns="http://docbook.org/ns/docbook"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xmlns:xs="http://www.w3.org/2001/XMLSchema"
         xmlns:xlink="http://www.w3.org/1999/xlink"
         xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:ns="http://docbook.org/ns/docbook">
  <title>Compute bond lengths</title>

  <para>
    This modifier computes the length of each bond and stores the value in a new bond property named <code>Length</code>. 
  </para>

  <para>
    A typical use case of this modifier is in conjunction with the <link linkend="particles.modifiers.color_coding">Color Coding</link>
    modifier to visualize the length of each bond. Furthermore, the <link linkend="particles.modifiers.histogram">Histogram</link>
    lets you plot the overall distribution of bond lengths in a system computed by this modifier.
  </para>

  <para>
    Note that, depending on the situation, the <link linkend="particles.modifiers.coordination_analysis">Coordination Analysis</link> modifier may provide an
    alternative. It computes the distribution of neighbor distances in a system of particles without explicit bonds.
  </para>

  <simplesect>
    <title>See also</title>
    <para>
      <link xlink:href="python/modules/ovito_modifiers.html#ovito.modifiers.ComputeBondLengthsModifier"><classname>ComputeBondLengthsModifier</classname> (Python API)</link>
    </para>
  </simplesect>  
    
</section>
